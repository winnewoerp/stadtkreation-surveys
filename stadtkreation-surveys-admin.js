// admin JS functions for Stadtkreation Surveys plugin by Johannes Bouchain - 02/2018

var $ = jQuery;
var newSurveyHTML, newQuestionHTML, newAnswerHTML;

$(document).ready(function(){
	// save form part html to variables, to be added on button click later
	newAnswerHTML = $('.stadtkreation-new-surveys .stadtkreation-new-answers').html();
	$('.stadtkreation-new-surveys .stadtkreation-new-answer').remove();
	newQuestionHTML = $('.stadtkreation-new-surveys .stadtkreation-new-questions').html();
	$('.stadtkreation-new-surveys .stadtkreation-new-question').remove();
	newSurveyHTML = $('.stadtkreation-new-surveys').html();
	$('.stadtkreation-new-surveys .stadtkreation-new-survey').remove();
	
	$('.button-secondary.stadtkreation-add-survey-button').click(function(e){ surveyButton(e, this); });
	
	// hide fields for new questions/answer possibilities in existing surveys initially
	$('.stadtkreation-existing-survey .stadtkreation-new-question, .stadtkreation-existing-survey .stadtkreation-new-answer').hide();
	
	$('.stadtkreation-existing-survey .button-secondary.stadtkreation-add-question-button').click(function(e){ existingSurveyQuestionButton(e, this); });	
	$('.stadtkreation-existing-survey .button-secondary.stadtkreation-add-answer-button').click(function(e){ existingSurveyAnswerButton(e, this); });

	// existing surveys action links
	$('.stadtkreation-surveys-dashboard .action-buttons .show-survey-details a').on('click',function(e){
		e.preventDefault();
		$(this).blur();
		$(this).closest('.stadtkreation-existing-survey, .survey-results').find('.content-box').slideToggle();
	});
	
	$('.stadtkreation-surveys-dashboard .action-buttons .delete-buttons a.delete').on('click',function(e){
		e.preventDefault();
		$(this).blur();
		$(this).closest('.delete-buttons').find('.marked').show();
		$(this).hide();
		$(this).closest('.box').addClass('to-be-deleted');
		if($(this).closest('.stadtkreation-existing-answer').length) $(this).closest('.box').find('.delete-answer-field').val(1);
		else {
			if($(this).closest('.stadtkreation-existing-question').length) $(this).closest('.box').find('.delete-question-field').val(1);
			else $(this).closest('.box').find('.delete-survey-field').val(1);
		}
	});
	
	$('.stadtkreation-surveys-dashboard .action-buttons .delete-buttons a.marked').on('click',function(e){
		e.preventDefault();
		$(this).blur();
		$(this).closest('.delete-buttons').find('.delete').show();
		$(this).hide();
		$(this).closest('.box').removeClass('to-be-deleted');
		if($(this).closest('.stadtkreation-existing-answer').length) $(this).closest('.box').find('.delete-answer-field').val(0);
		else {
			if($(this).closest('.stadtkreation-existing-question').length) $(this).closest('.box').find('.delete-question-field').val(0);
			else $(this).closest('.box').find('.delete-survey-field').val(0);
		}
	});
	
	// initially load survey fields for unsaved survey after form has been sent and form has errors
	if($('.stadtkreation-surveys-dashboard form.error').length) {
		var surveys = ($('.hidden-fields input[name="survey-questions"]').val() ? $('.hidden-fields input[name="survey-questions"]').val().split(',').length : []);
		var surveyQuestions = ($('.hidden-fields input[name="survey-questions"]').val() ? $('.hidden-fields input[name="survey-questions"]').val().split(',') : []);
		var surveyAnswers = ($('.hidden-fields input[name="survey-question-answers"]').val() ? $('.hidden-fields input[name="survey-question-answers"]').val().split(',') : []);
		if(surveys) {
			for(var i=1;i<=surveys;i++) surveyButton('', $('.button-secondary.stadtkreation-add-survey-button'), true);
			for(var i = 1; i<=surveyAnswers.length; i++) {
				for(var j = 1; j<=surveyQuestions[i-1]; j++) {
					questionButton('', $('.button-secondary.stadtkreation-add-question-button').last(), true);
					for(var k = 1; k<=surveyAnswers[j-1]; k++) {
						answerButton('', $('.button-secondary.stadtkreation-add-answer-button').last(), true);
					}
				}
			}
		}
		updateCountFields();
	}
	
	// load values into form if sent data
	if($('.sent-form-data').length) {
		if($('.stadtkreation-new-survey-title').length) {
			var dataFormValues = $('.stadtkreation-new-survey-title').attr('data-form-values').split('|*|');
			var i=0;
			$('input[name="stadtkreation-new-survey-title[]"]').each(function(){
				$(this).val(dataFormValues[i]);
				if(dataFormValues[i]=='') $(this).addClass('error');
				i++;
			});
		}
		if($('.stadtkreation-new-question-intro').length) {
			var dataFormValues = $('.stadtkreation-new-question-intro').attr('data-form-values').split('|*|');
			var i=0;
			$('textarea[name="stadtkreation-new-question-intro[]"]').each(function(){
				$(this).val(dataFormValues[i]);
				i++;
			});
		}
		if($('.stadtkreation-new-question-title').length) {
			var dataFormValues = $('.stadtkreation-new-question-title').attr('data-form-values').split('|*|');
			var i=0;
			$('input[name="stadtkreation-new-question-title[]"]').each(function(){
				$(this).val(dataFormValues[i]);
				if(dataFormValues[i]=='') $(this).addClass('error');
				i++;
			});
		}
		if($('.stadtkreation-new-answer-title').length) {
			var dataFormValues = $('.stadtkreation-new-answer-title').attr('data-form-values').split('|*|');
			var i=0;
			$('input[name="stadtkreation-new-answer-title[]"]').each(function(){
				$(this).val(dataFormValues[i]);
				if(dataFormValues[i]=='') $(this).addClass('error');
				i++;
			});
		}
				
	}
	
	$('.stadtkreation-new-surveys').show();
		
});

function surveyButton(e, element, noUpdate) {
	if(e) e.preventDefault();
	$(element).blur();
	$('.stadtkreation-new-surveys').append(newSurveyHTML);
	$('.button-secondary.stadtkreation-add-question-button').off();
	$('.button-secondary.stadtkreation-add-question-button').on('click',function(e){ questionButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .ask-cancel').on('click',function(e){ cancelButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .definitely-cancel').on('click',function(e){ definitelyCancelButton(e, this); });
	$(element).hide();
	
	if(!noUpdate) updateCountFields();

}

function questionButton(e, element, noUpdate) {
	if(e) e.preventDefault();
	$(element).blur();
	$(element).closest('.stadtkreation-new-survey').find('.stadtkreation-new-questions').append(newQuestionHTML);
	$('.button-secondary.stadtkreation-add-answer-button').off();
	$('.button-secondary.stadtkreation-add-answer-button').on('click',function(e){ answerButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .ask-cancel').on('click',function(e){ cancelButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .definitely-cancel').on('click',function(e){ definitelyCancelButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .move-up').on('click',function(e){ moveUpButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .move-down').on('click',function(e){ moveDownButton(e, this); });
	
	if(!noUpdate) updateCountFields();
	
}

// separate functions for new survey add question/answer buttons
function answerButton(e, element, noUpdate) {
	if(e) e.preventDefault();
	$(element).blur();
	$(element).closest('.stadtkreation-new-question').find('.stadtkreation-new-answers').append(newAnswerHTML);
	$('.stadtkreation-new-surveys .action-buttons .ask-cancel').on('click',function(e){ cancelButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .definitely-cancel').on('click',function(e){ definitelyCancelButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .move-up').on('click',function(e){ moveUpButton(e, this); });
	$('.stadtkreation-new-surveys .action-buttons .move-down').on('click',function(e){ moveDownButton(e, this); });
	
	if(!noUpdate) updateCountFields();
}

function existingSurveyQuestionButton(e, element) {
	e.preventDefault();
	var questionContent = $(element).closest('.stadtkreation-new-questions').find('.stadtkreation-new-question').last();
	if(questionContent.is(':visible')) {
		var addedQuestionContent = questionContent.clone();
		addedQuestionContent.hide();
		questionContent.after(addedQuestionContent);
		addedQuestionContent.slideDown();
		addedQuestionContent.find('.button-secondary.stadtkreation-add-answer-button').click(function(e){ existingSurveyAnswerButton(e, this); });
		addedQuestionContent.find('.order-number').val(parseInt(addedQuestionContent.find('.order-number').val())+1);
		addedQuestionContent.find('input:not([type="hidden"]),textarea').val('');
	}
	else questionContent.slideDown();
}

// separate functions for existing survey add question/answer buttons
function existingSurveyAnswerButton(e, element) {
	e.preventDefault();
	element.blur();
	var answerContent = $(element).closest('.stadtkreation-new-answers').find('.stadtkreation-new-answer').last();
	if(answerContent.is(':visible')) {
		var addedAnswerContent = answerContent.clone();
		addedAnswerContent.hide();
		answerContent.after(addedAnswerContent);
		addedAnswerContent.slideDown();
		addedAnswerContent.find('.order-number').val(parseInt(addedAnswerContent.find('.order-number').val())+1);
		addedAnswerContent.find('input:not([type="hidden"]),textarea').val('');
	}
	else answerContent.slideDown();
}

function cancelButton(e, element) {
	e.preventDefault();
	element.blur();
	$(element).blur();
	$(element).closest('.action-buttons').find('.definitely-cancel').show();
	setTimeout(function(element){ $('.definitely-cancel').hide(); },8000);
	
}

function definitelyCancelButton(e, element) {
	e.preventDefault();
	$(element).blur();
	$(element).closest('.box').remove();
	if(!$('.stadtkreation-new-survey').length) $('.stadtkreation-add-survey-button').show();
	
	updateCountFields();
}

function updateCountFields() {
	var questionsPerSurvey = new Array();
	var questionsPerSurveyString = '';
	$('.stadtkreation-new-survey').each(function(){
		questionsPerSurvey.push($(this).find('.stadtkreation-new-question').length);
		questionsPerSurveyString = questionsPerSurvey.join(',');
	});
	var answersPerQuestion = new Array();
	var answersPerQuestionString = '';
	$('.stadtkreation-new-question').each(function(){
		answersPerQuestion.push($(this).find('.stadtkreation-new-answer').length);
		answersPerQuestionString = answersPerQuestion.join(',');
	});
	$('input[name="survey-questions"]').val(questionsPerSurveyString);
	$('input[name="survey-question-answers"]').val(answersPerQuestionString);
}

function moveUpButton(e, element) {
	if(e) e.preventDefault();
	element.blur();
	var element1 = $(element).closest('.box');
	var element2 = element1.prev();
	if(element2) element1.after(element2);
	
	updateCountFields();
}

function moveDownButton(e, element) {
	if(e) e.preventDefault();
	element.blur();
	var element1 = $(element).closest('.box');
	var element2 = element1.next();
	if(element2) element1.before(element2);
	
	updateCountFields();	
}
	