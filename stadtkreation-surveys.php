<?php
/*
Plugin Name: Stadtkreation Surveys
Plugin URI: http://www.stadtkreation.de/wordpress-plugins-und-themes
Description: Stadtkreation Surveys plugin for flexible surveys.
Version: 0.5
Author: Johannes Bouchain
Text Domain: stadtkreation-surveys
Domain Path: /languages/
Author URI: http://www.stadtkreation.de/ueber-uns
*/
__('Stadtkreation Surveys','stadtkreation-surveys');
__('Stadtkreation Surveys plugin for flexible surveys.','stadtkreation-surveys');

// plugin textdomain definition
function stadtkreation_surveys_textdomain() {
	$plugin_dir = basename(dirname(__FILE__));
	load_plugin_textdomain( 'stadtkreation-surveys', false, $plugin_dir.'/languages/' );
}
add_action('plugins_loaded', 'stadtkreation_surveys_textdomain');

// enqueue scripts and styles
function stadtkreation_surveys_enqueue_styles() {
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_script( 'jquery');
	wp_enqueue_style( 'stadtkreation-surveys-styles', plugins_url('', __FILE__).'/stadtkreation-surveys.css' );
	wp_enqueue_script( 'stadtkreation-surveys-script', plugins_url('', __FILE__).'/stadtkreation-surveys.js', '', '20180904', true );
	wp_enqueue_script( 'stadtkreation-surveys-recaptcha-script', 'https://www.google.com/recaptcha/api.js' );
	
}
add_action('wp_enqueue_scripts','stadtkreation_surveys_enqueue_styles');

// enqueue plugn admin scripts and styles
function stadtkreation_surveys_admin_scripts( ) {
	wp_enqueue_style( 'stadtkreation-surveys-admin-styles', plugins_url('', __FILE__).'/stadtkreation-surveys-admin.css' );
	wp_enqueue_script( 'stadtkreation-surveys-admin-script', plugins_url('', __FILE__).'/stadtkreation-surveys-admin.js', '', '20180226', true );
	wp_enqueue_script( 'stadtkreation-surveys-admin-script', plugins_url('', __FILE__).'/stadtkreation-surveys-admin.js', '', '20180226', true );
}
add_action( 'admin_enqueue_scripts', 'stadtkreation_surveys_admin_scripts' );

// add options
add_option('stadtkreation-survey-settings');

// register settings
function stadtkreation_surveys_register_settings() {
	register_setting('stadtkreation-surveys-settings-group','stadtkreation-survey-settings');
}

// options page
function stadtkreation_surveys_settings_page() {
	global $wpdb, $question_types;
	$plugin_options = get_option('stadtkreation-survey-settings');
	
	// define question types
	$question_types = array(
		'text' => __('Single line text','stadtkreation-surveys'),
		'textarea' => __('Multi-line text','stadtkreation-surveys'),
		'radio' => __('Multiple choice, one selection','stadtkreation-surveys'),
		'checkbox' => __('Multiple choice, multiple selections','stadtkreation-surveys'),
		'checkbox3' => __('Multiple choice, max. 3 selections','stadtkreation-surveys')
	);
	
	// table names of custom plugin DB tables
	$table_name_no_prefix =  'stadtkreation_surveys';
	$table_name_surveys = $wpdb->prefix . $table_name_no_prefix;
	$table_name_no_prefix =  'stadtkreation_survey_questions';
	$table_name_questions = $wpdb->prefix . $table_name_no_prefix;
	$table_name_no_prefix =  'stadtkreation_survey_answer_possibilities';
	$table_name_answer_possibilites = $wpdb->prefix . $table_name_no_prefix;
	$table_name_no_prefix =  'stadtkreation_survey_user_answers';
	$table_name_survey_user_answers = $wpdb->prefix . $table_name_no_prefix;
	
	echo '
	<div class="wrap stadtkreation-surveys-dashboard">';
	
	// check for errors after new survey form has been sent
	$err = '';
	if(isset($_POST['new-survey-form-sent'])) {
		$fields_error_text = __('The marked fields need to be filled.','stadtkreation-surveys');
		if(isset($_POST['stadtkreation-new-survey-title']) && !isset($_POST['stadtkreation-new-question-title'])) $err[]=__('Your survey needs to have at least one question.','stadtkreation-surveys');
		if(isset($_POST['stadtkreation-new-survey-title'])) foreach($_POST['stadtkreation-new-survey-title'] as $single_field) if(trim($single_field)=='') $err['fields']=$fields_error_text;
		if(isset($_POST['stadtkreation-new-question-title'])) foreach($_POST['stadtkreation-new-question-title'] as $single_field) if(trim($single_field)=='') $err['fields']=$fields_error_text;
		if(isset($_POST['stadtkreation-new-answer-title'])) foreach($_POST['stadtkreation-new-answer-title'] as $single_field) if(trim($single_field)=='') $err['fields']=$fields_error_text;
	};
	
	echo '
		<h2>'.__('Preferences for Stadtkreation Surveys module','stadtkreation-surveys').'</h2>';
		
	// save form data for new survey if no errors
	if(isset($_POST['new-survey-form-sent']) && !$err) {
		// group sent answer possibility data by question
		if(isset($_POST['stadtkreation-new-answer-title'])) {
			$count = 0;
			$count2 = 0;
			$answer_titles = array();
			$survey_question_answers = explode(',',$_POST['survey-question-answers']);
			foreach($survey_question_answers as $question_answer_counter) {
				for($i = 0;$i<$survey_question_answers[$count];$i++) {
					$answer_titles[$count][] = $_POST['stadtkreation-new-answer-title'][$count2];
					$count2++;
				}
				$count++;
			}

		}
		// save new survey
		// TODO: Change later for multiple survey insert
		$success_printed = false;
		if(isset($_POST['stadtkreation-new-survey-title'])) {
			$data = array();
			$data['survey_creation_date'] = date('Y-m-d H:i',current_time( 'timestamp' ));
			$data['survey_title'] = $_POST['stadtkreation-new-survey-title'][0];
			$inserted = $wpdb->insert( $table_name_surveys, $data);
			$last_survey_id = $wpdb->insert_id;
			if($inserted) {
				$count = 0;
				foreach($_POST['stadtkreation-new-question-title'] as $question_title) {
					if($question_title) {
						$data = array();
						$data['survey_id'] = $last_survey_id;
						$data['survey_question_order'] = $count;
						$data['survey_question_intro'] = $_POST['stadtkreation-new-question-intro'][$count];
						$data['survey_question_type'] = $_POST['stadtkreation-new-question-type'][$count];
						$data['survey_question_title_before'] = $_POST['stadtkreation-new-question-title-before'][$count];
						$data['survey_question_title_export'] = $_POST['stadtkreation-new-question-title-export'][$count];
						$data['survey_question_optional'] = $_POST['stadtkreation-new-question-optional'][$count];
						$data['survey_question_next_question_default'] = $_POST['stadtkreation-new-question-next-default'][$count];
						$data['survey_question_title'] = $question_title;
						$inserted = $wpdb->insert( $table_name_questions, $data);
						$last_question_id = $wpdb->insert_id;
						if($inserted && isset($_POST['stadtkreation-new-answer-title'])) {
							$answer_titles_array = $answer_titles[$count];
							if($answer_titles_array) {
								$count2 = 0;
								foreach($answer_titles_array as $single_answer_title) {
									$data = array();
									$data['survey_id'] = $last_survey_id;
									$data['survey_question_id'] = $last_question_id;
									$data['survey_answer_possibility_order'] = $count2;
									$data['survey_answer_possibility_title'] = $single_answer_title;
									$inserted = $wpdb->insert( $table_name_answer_possibilites, $data);
									if($inserted) $new_survey_success = true;
									$count2++;
								}
							}
						}
						else  $new_survey_success = true;
						$count++;
					}
				}
			}
		}
	}
	
	// sent data handling for existing surveys
	if(isset($_POST['existing-survey-form-sent'])) {
		// delete surveys that were marked for deletion
		foreach($_POST['stadtkreation-delete-survey'] as $key => $value) {
			if($value==1) {
				$wpdb->delete($table_name_surveys, array( 'id' => $key ) );
				$wpdb->delete($table_name_questions, array( 'survey_id' => $key ) );
				$wpdb->delete($table_name_answer_possibilites, array( 'survey_id' => $key ) );
			}
		}
		if(isset($_POST['stadtkreation-delete-question'])) foreach($_POST['stadtkreation-delete-question'] as $key => $value) {
			if($value==1) {
				$wpdb->delete($table_name_questions, array( 'id' => $key ) );
				$wpdb->delete($table_name_answer_possibilites, array( 'survey_question_id' => $key ) );
			}
		}
		if(isset($_POST['stadtkreation-delete-answer'])) foreach($_POST['stadtkreation-delete-answer'] as $key => $value) {
			if($value==1) {
				$wpdb->delete($table_name_answer_possibilites, array( 'id' => $key ) );
			}
		}
		
		// update fields
		// TODO: Create array with $_POST variables and DB field name and loop through it 
		if(isset($_POST['stadtkreation-existing-survey-title'])) foreach($_POST['stadtkreation-existing-survey-title'] as $key => $value) $wpdb->update($table_name_surveys, array( 'survey_title' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-question-intro'])) foreach($_POST['stadtkreation-existing-question-intro'] as $key => $value) $wpdb->update($table_name_questions, array( 'survey_question_intro' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-question-title'])) foreach($_POST['stadtkreation-existing-question-title'] as $key => $value) $wpdb->update($table_name_questions, array( 'survey_question_title' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-question-title-export'])) foreach($_POST['stadtkreation-existing-question-title-export'] as $key => $value) $wpdb->update($table_name_questions, array( 'survey_question_title_export' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-question-title-before'])) foreach($_POST['stadtkreation-existing-question-title-before'] as $key => $value) $wpdb->update($table_name_questions, array( 'survey_question_title_before' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-question-type'])) foreach($_POST['stadtkreation-existing-question-type'] as $key => $value) $wpdb->update($table_name_questions, array( 'survey_question_type' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-question-next-default'])) foreach($_POST['stadtkreation-existing-question-next-default'] as $key => $value) $wpdb->update($table_name_questions, array( 'survey_question_next_question_default' => $value), array('id' => $key));
		// special case: checkbox values must be rewritten to zero before updating
		$wpdb->query($wpdb->prepare("UPDATE ".$table_name_questions." SET survey_question_optional = %s WHERE id > 0",0,1));
		if(isset($_POST['stadtkreation-existing-question-optional'])) foreach($_POST['stadtkreation-existing-question-optional'] as $key => $value) $wpdb->update($table_name_questions, array( 'survey_question_optional' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-question-order'])) foreach($_POST['stadtkreation-existing-question-order'] as $key => $value) $wpdb->update($table_name_questions, array( 'survey_question_order' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-answer-possibility-title'])) foreach($_POST['stadtkreation-existing-answer-possibility-title'] as $key => $value) $wpdb->update($table_name_answer_possibilites, array( 'survey_answer_possibility_title' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-answer-possibility-order'])) foreach($_POST['stadtkreation-existing-answer-possibility-order'] as $key => $value) $wpdb->update($table_name_answer_possibilites, array( 'survey_answer_possibility_order' => $value), array('id' => $key));
		if(isset($_POST['stadtkreation-existing-answer-possibility-next-question'])) foreach($_POST['stadtkreation-existing-answer-possibility-next-question'] as $key => $value) $wpdb->update($table_name_answer_possibilites, array( 'survey_answer_possibility_next_question' => $value), array('id' => $key));
		
		// add question and answer possibilities
		foreach($_POST['stadtkreation-added-question-title'] as $key => $value) {
			$data['survey_id'] = $key;
			$countfields = 0;
			foreach($value as $title) {
				if($title) {
					$data['survey_question_title'] = $title;
					$data['survey_question_title_export'] = $_POST['stadtkreation-added-question-title-export'][$key][$countfields];
					$data['survey_question_title_before'] = $_POST['stadtkreation-added-question-title-before'][$key][$countfields];
					$data['survey_question_intro'] = $_POST['stadtkreation-added-question-intro'][$key][$countfields];
					$data['survey_question_type'] = $_POST['stadtkreation-added-question-type'][$key][$countfields];
					$data['survey_question_next_question_default'] = $_POST['stadtkreation-added-question-next-default'][$key][$countfields];
					$data['survey_question_optional'] = $_POST['stadtkreation-added-question-optional'][$key][$countfields];
					$data['survey_question_order'] = $_POST['stadtkreation-added-question-order'][$key][$countfields];
					$inserted = $wpdb->insert( $table_name_questions, $data);
					$countfields++;
				}
			}
		}
		
		foreach($_POST['stadtkreation-added-answer-possibility-title'] as $key => $value) {
			$data['survey_id'] = $key;
			foreach($value as $inner_key => $inner_value) {
				$data['survey_question_id'] = $inner_key;
				$countfields = 0;
				foreach($inner_value as $title) {
					if($title) {
						$data['survey_answer_possibility_order'] = $_POST['stadtkreation-added-answer-possibility-order'][$key][$inner_key][$countfields];
						$data['survey_answer_possibility_title'] = $title;
						$data['survey_answer_possibility_next_question'] = $_POST['stadtkreation-added-answer-possibility-next-question'][$count];
						$inserted = $wpdb->insert( $table_name_answer_possibilites, $data);
						$countfields++;
					}
				}
			}
		}
	}
	
	if($inserted) $new_survey_success = true;
		
		// load existing surveys
	echo '
			<h3>'.__('Existing surveys','stadtkreation-surveys').'</h3>
			<div class="stadtkreation-existing-surveys">';
	$survey_sql = 'SELECT * FROM '.$table_name_surveys.' ORDER BY survey_creation_date ASC';
	$survey_rows = $wpdb->get_results($survey_sql);
	if($survey_rows) echo '
			<form method="post">';
	if($survey_rows) echo '
				<p><input type="submit" class="button-primary" value="'.__('Save changes for existing surveys','stadtkreation-surveys').'" /></p>';
	foreach($survey_rows as $survey_row) {
		echo '
				<div class="stadtkreation-existing-survey box">
					<div class="title-row">
						<strong>[stadtkreation-survey id="'.$survey_row->id.'"]</strong> <label for="stadtkreation-existing-survey-title['.$survey_row->id.']">'.__('Survey title:','stadtkreation-surveys').'</label> <input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-survey-title['.$survey_row->id.']" value="'.$survey_row->survey_title.'" />
						<p class="action-buttons"><span class="delete-buttons"><a class="delete" href="#">'.__('Mark for deletion','stadtkreation-surveys').'</a><a class="marked" href="#"><strong>'.__('Marked for deletion','stadtkreation-surveys').'</strong></a></span> &nbsp; <span class="show-survey-details"><a href="#">'.__('Show survey details','stadtkreation-surveys').'</a></span></p>
						<div class="clearfix"></div>
						<input type="hidden" class="delete-survey-field" name="stadtkreation-delete-survey['.$survey_row->id.']" value="0" />
						
					</div>
					<div class="content-box">';
		$question_sql = 'SELECT * FROM '.$table_name_questions.' WHERE survey_id = '.$survey_row->id.' ORDER BY survey_question_order ASC';	
		$question_rows = $wpdb->get_results($question_sql);
		$question_order_number = 0;		
		foreach($question_rows as $question_row) {
			echo '
						<div class="stadtkreation-existing-question box">
							<p>'.__('Question ID:','stadtkreation-surveys').' <strong>'.$question_row->id.'</strong></p>
							<p><label for="stadtkreation-existing-question-title['.$question_row->id.']">'.__('Question title','stadtkreation-surveys').'</label><br />
							<input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-question-title['.$question_row->id.']" value="'.$question_row->survey_question_title.'" /></p>
							<p><label for="stadtkreation-existing-question-title-export['.$question_row->id.']">'.__('Optional export title','stadtkreation-surveys').'</label><br />
							<input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-question-title-export['.$question_row->id.']" value="'.$question_row->survey_question_title_export.'" /></p>
							<p><label for="stadtkreation-existing-question-title-before['.$question_row->id.']">'.__('Optional section title (above question)','stadtkreation-surveys').'</label><br />
							<input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-question-title-before['.$question_row->id.']" value="'.$question_row->survey_question_title_before.'" /></p>
							<p><label for="stadtkreation-existing-question-intro['.$question_row->id.']">'.__('Question intro','stadtkreation-surveys').'</label><br />
							<textarea class="stadtkreation-textinput" name="stadtkreation-existing-question-intro['.$question_row->id.']">'.$question_row->survey_question_intro.'</textarea></p>
							<p><label for="stadtkreation-existing-question-type['.$question_row->id.']">'.__('Question type','stadtkreation-surveys').'</label><br />
								<select name="stadtkreation-existing-question-type['.$question_row->id.']">';
								foreach($question_types as $question_type => $question_type_output) echo '
									<option value="'.$question_type.'"'.($question_row->survey_question_type == $question_type ? ' selected="selected"' : '').'>'.$question_type_output.'</option>';
							echo '
								</select>
							</p>
							<p><label for="stadtkreation-existing-question-next-default['.$question_row->id.']">'.__('ID of next question (optional)','stadtkreation-surveys').'</label><br />
							<input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-question-next-default['.$question_row->id.']" value="'.$question_row->survey_question_next_question_default.'" /></p>
							<p><label for="stadtkreation-existing-question-optional['.$question_row->id.']">'.__('Optional question','stadtkreation-surveys').'</label><br />
							<input type="checkbox" class="stadtkreation-checkbox" name="stadtkreation-existing-question-optional['.$question_row->id.']"'.($question_row->survey_question_optional==1 ? ' checked="checked"' : '').' value="1" /></p>
							<p><label for="stadtkreation-existing-question-order['.$question_row->id.']">'.__('Question order number','stadtkreation-surveys').'</label><br />
							<input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-question-order['.$question_row->id.']" value="'.$question_row->survey_question_order.'" /></p>
							<p class="action-buttons"><span class="delete-buttons"><a class="delete" href="#">'.__('Mark for deletion','stadtkreation-surveys').'</a><a class="marked" href="#"><strong>'.__('Marked for deletion','stadtkreation-surveys').'</strong></a></span></p>
							<div class="clearfix"></div>
							<input type="hidden" class="delete-question-field" name="stadtkreation-delete-question['.$question_row->id.']" value="0" />';
			$question_order_number = $question_row->survey_question_order;	
			$answer_sql = 'SELECT * FROM '.$table_name_answer_possibilites.' WHERE survey_question_id = '.$question_row->id.' ORDER BY survey_answer_possibility_order ASC';	
			$answer_rows = $wpdb->get_results($answer_sql);
			$answer_order_number = 0;
			foreach($answer_rows as $answer_row) {
				echo '
							<div class="stadtkreation-existing-answer box">
								<p><label for="stadtkreation-existing-answer-possibility-title['.$answer_row->id.']">'.__('Answer possibility title','stadtkreation-surveys').'</label><br />
								<input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-answer-possibility-title['.$answer_row->id.']" value="'.$answer_row->survey_answer_possibility_title.'" /></p>
								<p><label for="stadtkreation-existing-answer-possibility-order['.$answer_row->id.']">'.__('Answer order number','stadtkreation-surveys').'</label><br />
								<input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-answer-possibility-order['.$answer_row->id.']" value="'.$answer_row->survey_answer_possibility_order.'" /></p>
								<p><label for="stadtkreation-existing-answer-possibility-next-question['.$answer_row->id.']">'.__('ID of next question','stadtkreation-surveys').'</label><br />
								<input type="text" class="stadtkreation-textinput" name="stadtkreation-existing-answer-possibility-next-question['.$answer_row->id.']" value="'.$answer_row->survey_answer_possibility_next_question.'" /></p>
								<p class="action-buttons"><span class="delete-buttons"><a class="delete" href="#">'.__('Mark for deletion','stadtkreation-surveys').'</a><a class="marked" href="#"><strong>'.__('Marked for deletion','stadtkreation-surveys').'</strong></a></span></p>
								<div class="clearfix"></div>
								<input type="hidden" class="delete-answer-field" name="stadtkreation-delete-answer['.$answer_row->id.']" value="0" />
							</div>';
							$answer_order_number = $answer_row->survey_answer_possibility_order;
			}
			echo '
							<div class="stadtkreation-new-answers">
								<div class="stadtkreation-new-answer">
									<h3>'.__('New answer possibility','stadtkreation-surveys').'</h3>
									<p>
										<label>'.__('New answer possibility title','stadtkreation-surveys').'</label><br />
										<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-added-answer-possibility-title['.$survey_row->id.']['.$question_row->id.'][]" />
										<input class="order-number" type="hidden" name="stadtkreation-added-answer-possibility-order['.$survey_row->id.']['.$question_row->id.'][]" value="'.($answer_order_number+1).'" />
									</p>
								</div>
								<p><button class="button-secondary stadtkreation-add-answer-button">+ '.__('Add answer possibility','stadtkreation-surveys').'</button></p>
							</div>
							
						</div>';
		}					
		echo '
						<div class="stadtkreation-new-questions">
							<div class="stadtkreation-new-question">
								<h3>'.__('New question','stadtkreation-surveys').'</h3>
								<p>
									<label>'.__('New question title','stadtkreation-surveys').'</label><br />
									<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-added-question-title['.$survey_row->id.'][]" />
								</p>
								<p>
									<label>'.__('Optional export title','stadtkreation-surveys').'</label><br />
									<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-added-question-title-export['.$survey_row->id.'][]" />
								</p>
								<p>
									<label>'.__('Optional section title (above question)','stadtkreation-surveys').'</label><br />
									<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-added-question-title-before['.$survey_row->id.'][]" />
								</p>
								<p>
									<label>'.__('New question intro','stadtkreation-surveys').'</label><br />
									<textarea class="stadtkreation-textinput" cols="100" name="stadtkreation-added-question-intro['.$survey_row->id.'][]"></textarea>
								</p>
								<p>
									<label>'.__('New question type','stadtkreation-surveys').'</label><br />
									<select name="stadtkreation-added-question-type['.$survey_row->id.'][]">';
									foreach($question_types as $question_type => $question_type_output) echo '
										<option value="'.$question_type.'">'.$question_type_output.'</option>';
								echo '
									</select>
								</p>
								<p>
									<label>'.__('ID of next question (optional)','stadtkreation-surveys').'</label><br />
									<input class="stadtkreation-textinput" type="text" cols="5" name="stadtkreation-added-question-next-default['.$survey_row->id.'][]" />
								</p>
								<p>
									<label>'.__('Optional question','stadtkreation-surveys').'</label><br />
									<input type="checkbox" class="stadtkreation-checkbox" name="stadtkreation-added-question-optional['.$survey_row->id.'][]" value="1" />
								</p>
								<input class="order-number" type="hidden" name="stadtkreation-added-question-order['.$survey_row->id.'][]" value="'.($question_order_number+1).'" />
								<p><strong>'.__('Please add new answer possibilities to new questions after saving!','stadtkreation-surveys').'</strong></p>
								<!--<div class="stadtkreation-new-answers">
									<div class="stadtkreation-new-answer">
										<h3>'.__('New answer possibility','stadtkreation-surveys').'</h3>
										<p>
											<label>'.__('New answer possibility title','stadtkreation-surveys').'</label><br />
											<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-added-answer-possibility-title['.$survey_row->id.']['.$question_row->id.'][]" />
											<input class="order-number" type="hidden" name="stadtkreation-added-answer-possibility-order['.$survey_row->id.']['.$question_row->id.'][]" value="'.($answer_order_number+1).'" />
										</p>
									</div>
									<p><button class="button-secondary stadtkreation-add-answer-button">+ '.__('Add answer possibility','stadtkreation-surveys').'</button></p>
								</div>-->
							</div>
							<p><button class="button-secondary stadtkreation-add-question-button">+ '.__('Add question','stadtkreation-surveys').'</button></p>
						</div>

					</div>
				</div>';
	}
	if($survey_rows) echo '
				<p><input type="submit" class="button-primary" value="'.__('Save changes for existing surveys','stadtkreation-surveys').'" /></p>
				<input type="hidden" name="existing-survey-form-sent" value="1" />
			</form>';
	else echo '
			<p class="stadtkreation-no-existing-surveys"><em>'.__('No existing surveys','stadtkreation-surveys').'</em></p>';
	echo '
		</div>
		<form method="post"'.($err ? ' class="error"' : '').'>';
		
	// output form data for JS field fill
	if($err) {
		echo '
			<span class="sent-form-data stadtkreation-new-survey-title" data-form-values="'.implode('|*|',$_POST['stadtkreation-new-survey-title']).'"></span>';
		if(isset($_POST['stadtkreation-new-question-title'])) echo '
			<span class="sent-form-data stadtkreation-new-question-title" data-form-values="'.implode('|*|',$_POST['stadtkreation-new-question-title']).'"></span>';
		if(isset($_POST['stadtkreation-new-question-intro'])) echo '
			<span class="sent-form-data stadtkreation-new-question-intro" data-form-values="'.implode('|*|',$_POST['stadtkreation-new-question-intro']).'"></span>';
		if(isset($_POST['stadtkreation-new-answer-title'])) echo '
			<span class="sent-form-data stadtkreation-new-answer-title" data-form-values="'.implode('|*|',$_POST['stadtkreation-new-answer-title']).'"></span>';
	}
	echo '
			<h3>'.__('New survey','stadtkreation-surveys').'</h3>';
		
	if($new_survey_success) echo '
			<div class="message success-message">'.__('Your survey has been saved successfully.','stadtkreation-surveys').'</div>';

	// output error message if error
	if($err) {
		echo '
			<div class="message error-message">
				'.__('Your survey could not be saved. Please note the following errors:','stadtkreation-surveys').'
				<ul>';
		foreach($err as $single_err) echo '
					<li>'.$single_err.'</li>';
		echo '
				</ul>
			</div>';
	}
	echo '
			<div class="stadtkreation-new-surveys">
				<div class="stadtkreation-new-survey box">
					<h3>'.__('Data for new survey','stadtkreation-surveys').'</h3>
					<p>
						<label>'.__('Survey title','stadtkreation-surveys').'<br />
						<input class="stadtkreation-textinput" type="text" rows="100" name="stadtkreation-new-survey-title[]" />
					</p>
					<div class="stadtkreation-new-questions">
						<div class="stadtkreation-new-question box">
							<h3>'.__('New question','stadtkreation-surveys').'</h3>
							<p>
								<label>'.__('New question title','stadtkreation-surveys').'<br />
								<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-new-question-title[]" />
							</p>
							<p>
								<label>'.__('Optional export title','stadtkreation-surveys').'<br />
								<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-new-question-title-export[]" />
							</p>
							<p>
								<label>'.__('Optional section title (above question)','stadtkreation-surveys').'<br />
								<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-new-question-title-before[]" />
							</p>
							<p>
								<label>'.__('New question intro','stadtkreation-surveys').'<br />
								<textarea class="stadtkreation-textinput" cols="100" name="stadtkreation-new-question-intro[]"></textarea>
							</p>
							<p>
								<label>'.__('ID of next question (optional)','stadtkreation-surveys').'<br />
								<input class="stadtkreation-textinput" type="text" cols="5" name="stadtkreation-new-question-next-default[]" />
							</p>
							<p>
								<label>'.__('Optional question','stadtkreation-surveys').'<br />
								<input type="checkbox" class="stadtkreation-checkbox" name="stadtkreation-new-question-optional[]" value="1"></textarea>
							</p>
							<div class="stadtkreation-new-answers">
								<div class="stadtkreation-new-answer box">
									<h3>'.__('New answer possibility','stadtkreation-surveys').'</h3>
									<p>
										<label>'.__('New answer possibility title','stadtkreation-surveys').'<br />
									<input class="stadtkreation-textinput" type="text" cols="100" name="stadtkreation-new-answer-title[]" />
									</p>
									<p class="action-buttons"><a href="#" class="move-up">'.__('Move up','stadtkreation-surveys').'</a> &nbsp; <a href="#" class="move-down">'.__('Move down','stadtkreation-surveys').'</a> &nbsp; <a href="#" class="ask-cancel">'.__('Cancel creation of new answer possibility','stadtkreation-surveys').'</a> &nbsp; <a href="#" class="hidden definitely-cancel strong">'.__('Definitely cancel new answer possibility','stadtkreation-surveys').'</a></p>
									<div class="clearfix"></div>
								</div>
							</div>
							<p><button class="button-secondary stadtkreation-add-answer-button">+ '.__('Add answer possibility','stadtkreation-surveys').'</button></p>
							<p class="action-buttons"><a href="#" class="move-up">'.__('Move up','stadtkreation-surveys').'</a> &nbsp; <a href="#" class="move-down">'.__('Move down','stadtkreation-surveys').'</a> &nbsp; <a href="#" class="ask-cancel">'.__('Cancel creation of new question','stadtkreation-surveys').'</a><br /><a href="#" class="hidden definitely-cancel strong">'.__('Definitely cancel new question','stadtkreation-surveys').'</a></p>
							<div class="clearfix"></div>
						</div>
					</div>
					<p><button class="button-secondary stadtkreation-add-question-button">+ '.__('Add question','stadtkreation-surveys').'</button></p>
					<p class="action-buttons"><a href="#" class="ask-cancel">'.__('Cancel creation of new survey','stadtkreation-surveys').'</a><br /><a href="#" class="hidden definitely-cancel strong">'.__('Definitely cancel new survey','stadtkreation-surveys').'</a></p>
					<div class="clearfix"></div>
				</div>
			</div>
			<p><button class="button-secondary stadtkreation-add-survey-button">+ '.__('Add survey','stadtkreation-surveys').'</button></p>
			<span class="hidden-fields">
				<input type="hidden" name="new-survey-form-sent" value="1" />
				<input type="hidden" name="survey-questions" value="'.(isset($_POST['survey-questions']) && $err ? $_POST['survey-questions'] : '').'" />
				<input type="hidden" name="survey-question-answers" value="'.(isset($_POST['survey-question-answers']) && $err ? $_POST['survey-question-answers'] : '').'" />
			</span>
			<p><input type="submit" class="button-primary" value="'.__('Save new survey','stadtkreation-surveys').'" /></p>
		</form>';
		
		// survey results
		echo '
		<div class="stadtkreation-survey-results box">';
		$survey_sql = 'SELECT * FROM '.$table_name_surveys.' ORDER BY survey_creation_date ASC';
		$survey_rows = $wpdb->get_results($survey_sql);
		if($survey_rows) echo '
			<h3>'.__('Survey results','stadtkreation-surveys').'</h3>';
		foreach($survey_rows as $survey_row) {
			echo '
			<div class="survey-results box">
				<div class="title-box">
					<strong>'.$survey_row->survey_title.'</strong> ('.__('Survey','stadtkreation-surveys').' '.$survey_row->id.')
					<form method="post">
					<p class="action-buttons"><span class="show-survey-details"><a class="button button-primary" href="#">'.__('Show results','stadtkreation-surveys').'</a></span>
					
						<input type="hidden" name="stadtkreation_surveys_download_csv" />
						<input type="hidden" name="csv_survey_id" value="'.$survey_row->id.'" />
						<input type="submit" class="button button-primary" value="'.__('Export as CSV file','stadtkreation-surveys').'" />
					</p>
					</form>
					<p class="clearfix"></p>					
				</div>
				<div class="content-box">';
				$results_sql = 'SELECT * FROM '.$table_name_survey_user_answers.' WHERE survey_id = '.$survey_row->id;
				// TODO: Make combination of user id and unique token possible (when logging changed during survey period) AND SIMPLIFY!
				$count_users = $wpdb->get_var('SELECT COUNT(DISTINCT survey_user_id, survey_user_answer_creation_date) FROM '.$table_name_survey_user_answers.' WHERE survey_id = '.$survey_row->id);
				$count_distinct_users = $wpdb->get_var('SELECT COUNT(DISTINCT survey_user_id) FROM '.$table_name_survey_user_answers.' WHERE survey_id = '.$survey_row->id);
				$count_tokens = $wpdb->get_var('SELECT COUNT(DISTINCT survey_answer_unique_token, survey_user_answer_creation_date) FROM '.$table_name_survey_user_answers.' WHERE survey_id = '.$survey_row->id);
				$count_distinct_tokens = $wpdb->get_var('SELECT COUNT(DISTINCT survey_answer_unique_token) FROM '.$table_name_survey_user_answers.' WHERE survey_id = '.$survey_row->id);
				echo '
					<p>';
				if($count_users) echo '
						<strong>'.__('Users:','stadtkreation-surveys').'</strong> '.$count_users.'<br />';
				if($count_distinct_users) echo '
					<strong>'.__('Distinct users:','stadtkreation-surveys').'</strong> '.$count_distinct_users.' <small>('.__('May be different from user count if double voting has not been switched off','stadtkreation-surveys').')</small></p>';
				if($count_tokens) echo '
						<strong>'.__('Users:','stadtkreation-surveys').'</strong> '.$count_tokens.'<br />';
				if($count_distinct_tokens) echo '
					<strong>'.__('Distinct users:','stadtkreation-surveys').'</strong> '.$count_distinct_tokens.' <small>('.__('May be different from user count if double voting has not been switched off','stadtkreation-surveys').')</small></p>';
					
				if($count_users > 0 || $count_tokens > 0) {
					echo '
					<h4>'.__('Results by question','stadtkreation-surveys').'</h4>';
					
					$question_sql = 'SELECT * FROM '.$table_name_questions.' WHERE survey_id = '.$survey_row->id.' ORDER BY survey_question_order ASC';	
					$question_rows = $wpdb->get_results($question_sql);
					$question_order_number = 0;		
					foreach($question_rows as $question_row) {
						$answer_sql = 'SELECT * FROM '.$table_name_answer_possibilites.' WHERE survey_question_id = '.$question_row->id.' ORDER BY survey_answer_possibility_order ASC';
						$answer_rows = $wpdb->get_results($answer_sql);
						$has_answer_possibilities = false;
						foreach($answer_rows as $answer_row) {
							$count_answers = $wpdb->get_var('SELECT COUNT(*) FROM '.$table_name_survey_user_answers.' WHERE survey_answer_possibility_id = '.$answer_row->id);
							if(!$has_answer_possibilities) {
								echo '
					<p><strong><big>'.$question_row->survey_question_title.'</big></strong></p>';
								if($question_row->survey_question_type == 'checkbox' || $question_row->survey_question_type == 'checkbox3') echo '
			<p><em>('.__('Multiple selection possible, therefore sum of percent values probably differs from 100','stadtkreation-surveys').')</em></p>';
							}
							echo '
					<p><strong>'.$answer_row->survey_answer_possibility_title.': '.$count_answers.'</strong><br />';
							$has_answer_possibilities = true;
							$count_users_new = $count_users;
							if(!$count_users_new) $count_users_new = $count_tokens;
							$percent = $count_answers / $count_users_new * 100;
							echo '<span class="percent-bar"><span class="inner" style="width:'.round($percent,2).'%"></span><span class="content">'.round($percent,2).'% </span></span></p>';
						}
					}
				}
			echo '
				</div>
			</div>';
		}
		echo '
		</div>';
	
		// settings form
		echo '
		<h3>'.__('General survey settings','stadtkreation-surveys').'</h3>
		<form method="post" action="options.php">';
		settings_fields('stadtkreation-surveys-settings-group' );
		do_settings_sections('stadtkreation-surveys-settings-group');
		echo '
			<p><label for="stadtkreation-settings-login-required"><input type="checkbox"'.(isset($plugin_options['login-required']) ? ($plugin_options['login-required'] ? ' checked="checked"' : '') : '').' name="stadtkreation-survey-settings[login-required]" id="stadtkreation-settings-login-required" />'.__('Login required','stadtkreation-surveys').'</p>
			<p><label for="stadtkreation-settings-double-voting-allowed"><input type="checkbox"'.(isset($plugin_options['double-voting-allowed']) ? ($plugin_options['double-voting-allowed'] ? ' checked="checked"' : '') : '').' name="stadtkreation-survey-settings[double-voting-allowed]" id="stadtkreation-settings-double-voting-allowed" />'.__('Double voting allowed','stadtkreation-surveys').'</p>
			<p><label for="stadtkreation-settings-send-notification"><input type="checkbox"'.(isset($plugin_options['send-notification']) ? ($plugin_options['send-notification'] ? ' checked="checked"' : '') : '').' name="stadtkreation-survey-settings[send-notification]" id="stadtkreation-settings-send-notification" />'.__('Send email admin notification on submit','stadtkreation-surveys').'</p>
			<p><label for="stadtkreation-settings-recaptcha-website-key">'.__('reCaptcha Website Key','stadtkreation-surveys').' <input type="text"'.(isset($plugin_options['recaptcha-website-key']) ? ($plugin_options['recaptcha-website-key'] ? ' value="'.$plugin_options['recaptcha-website-key'].'"' : '') : '').' name="stadtkreation-survey-settings[recaptcha-website-key]" id="stadtkreation-settings-recaptcha-website-key" /></p>
			<p><label for="stadtkreation-settings-recaptcha-secret-key">'.__('reCaptcha Secret Key','stadtkreation-surveys').' <input type="text"'.(isset($plugin_options['recaptcha-secret-key']) ? ($plugin_options['recaptcha-secret-key'] ? ' value="'.$plugin_options['recaptcha-secret-key'].'"' : '') : '').' name="stadtkreation-survey-settings[recaptcha-secret-key]" id="stadtkreation-settings-recaptcha-secret-key" /></p>
			<p><input type="submit" class="button-primary" value="'.__('Save settings','stadtkreation-surveys').'" /></p>
		</form>
	</div>';
}

// add setting sub menu link
function stadtkreation_surveys_create_menu() {

	//create new top-level menu
	add_options_page(__('Surveys preferences','stadtkreation-surveys'),__('Stadtkreation Surveys','stadtkreation-surveys'), 'manage_options', 'stadtkreation-surveys-preferences', 'stadtkreation_surveys_settings_page');
	
	//call register settings function
	add_action( 'admin_init', 'stadtkreation_surveys_register_settings' );
}
add_action('admin_menu', 'stadtkreation_surveys_create_menu');

// setup database table for Stadtkreation Surveys plugin
function stadtkreation_surveys_database_setup() {
	global $wpdb;
	
	// table stadtkreation_surveys
	$table_name_no_prefix =  'stadtkreation_surveys';
	$table_name = $wpdb->prefix . $table_name_no_prefix;
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name_no_prefix) {
		global $wpdb;
		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			survey_creation_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			survey_title tinytext NOT NULL,
			UNIQUE KEY id (id)
		);";
	
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
	
	// table stadtkreation_survey_questions
	$table_name_no_prefix =  'stadtkreation_survey_questions';
	$table_name = $wpdb->prefix . $table_name_no_prefix;
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name_no_prefix) {
		global $wpdb;
		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			survey_id int,
			survey_question_order int,
			survey_question_intro mediumtext,
			survey_question_title tinytext NOT NULL,
			survey_question_title_export tinytext,
			survey_question_title_before tinytext,
			survey_question_type tinytext,
			survey_question_optional int,
			survey_question_next_question_default int,
			survey_question_new_page int,
			UNIQUE KEY id (id)
		);";
	
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
	
	// table stadtkreation_survey_answers
	$table_name_no_prefix =  'stadtkreation_survey_answer_possibilities';
	$table_name = $wpdb->prefix . $table_name_no_prefix;
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name_no_prefix) {
		global $wpdb;
		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			survey_id int,
			survey_question_id int,
			survey_answer_possibility_order int,
			survey_answer_possibility_title tinytext NOT NULL,
			survey_answer_possibility_next_question int,
			UNIQUE KEY id (id)
		);";
	
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
	
	// table stadtkreation_survey_answers
	$table_name_no_prefix =  'stadtkreation_survey_user_answers';
	$table_name = $wpdb->prefix . $table_name_no_prefix;
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name_no_prefix) {
		global $wpdb;
		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			survey_user_answer_creation_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			survey_id int,
			survey_question_id int,
			survey_question_title tinytext,
			survey_answer_possibility_id int,
			survey_answer_possibility_text mediumtext,
			survey_user_id int,
			survey_answer_unique_token tinytext,
			UNIQUE KEY id (id)
		);";
	
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
}
add_action('after_setup_theme','stadtkreation_surveys_database_setup');

// shortcode [stadtkreation-survey id="XX"]
function output_stadtkreation_survey($atts,$content) {
	$a = shortcode_atts( array(
		'id' => '',
	), $atts );
	
	$output = '';
	$plugin_options = get_option('stadtkreation-survey-settings');
	
	global $wpdb;
	
	// table names of custom plugin DB tables
	$table_name_no_prefix =  'stadtkreation_surveys';
	$table_name_surveys = $wpdb->prefix . $table_name_no_prefix;
	$table_name_no_prefix =  'stadtkreation_survey_questions';
	$table_name_questions = $wpdb->prefix . $table_name_no_prefix;
	$table_name_no_prefix =  'stadtkreation_survey_answer_possibilities';
	$table_name_answer_possibilites = $wpdb->prefix . $table_name_no_prefix;
	$table_name_no_prefix =  'stadtkreation_survey_user_answers';
	$table_name_user_answers = $wpdb->prefix . $table_name_no_prefix;
	
	// ### SHOW SURVEY RESULTS
	
	if(in_array('results',$atts)) {
		$output .= '
		<div class="stadtkreation-survey-results">';
		$survey_sql = 'SELECT * FROM '.$table_name_surveys.' WHERE id = '.$a['id'].' ORDER BY survey_creation_date ASC';
		$survey_rows = $wpdb->get_results($survey_sql);
		if($survey_rows) $output .= '
			<h3>'.__('Survey results','stadtkreation-surveys').'</h3>';
		foreach($survey_rows as $survey_row) {
			$output .= '
			<p><strong>'.$survey_row->survey_title.'</strong> ('.__('Survey','stadtkreation-surveys').' '.$survey_row->id.')</p>';
				$results_sql = 'SELECT * FROM '.$table_name_user_answers.' WHERE survey_id = '.$survey_row->id;
				$count_users = $wpdb->get_var('SELECT COUNT(DISTINCT survey_user_id, survey_user_answer_creation_date) FROM '.$table_name_user_answers.' WHERE survey_id = '.$survey_row->id);
				$count_distinct_users = $wpdb->get_var('SELECT COUNT(DISTINCT survey_user_id) FROM '.$table_name_user_answers.' WHERE survey_id = '.$survey_row->id);
				$count_tokens = $wpdb->get_var('SELECT COUNT(DISTINCT survey_answer_unique_token, survey_user_answer_creation_date) FROM '.$table_name_user_answers.' WHERE survey_id = '.$survey_row->id);
				$count_distinct_tokens = $wpdb->get_var('SELECT COUNT(DISTINCT survey_answer_unique_token) FROM '.$table_name_user_answers.' WHERE survey_id = '.$survey_row->id);
				if($count_users) $output .= '
			<p><strong>'.__('Users:','stadtkreation-surveys').'</strong> '.$count_users.'</p>';
				if($count_tokens) $output .= '
			<p><strong>'.__('Users:','stadtkreation-surveys').'</strong> '.$count_tokens.'</p>';

				if($count_users > 0 || $count_tokens > 0) {
					$question_sql = 'SELECT * FROM '.$table_name_questions.' WHERE survey_id = '.$survey_row->id.' ORDER BY survey_question_order ASC';	
					$question_rows = $wpdb->get_results($question_sql);
					$question_order_number = 0;		
					foreach($question_rows as $question_row) {
						$answer_sql = 'SELECT * FROM '.$table_name_answer_possibilites.' WHERE survey_question_id = '.$question_row->id.' ORDER BY survey_answer_possibility_order ASC';	
						$answer_rows = $wpdb->get_results($answer_sql);
						$has_answer_possibilities = false;
						foreach($answer_rows as $answer_row) {
							if(!$has_answer_possibilities) {
								$output .= '
			<p><strong><big>'.$question_row->survey_question_title.'</big></strong></p>';
								if($question_row->survey_question_type == 'checkbox' || $question_row->survey_question_type == 'checkbox3') $output .= '
			<p><em>('.__('Multiple selection possible, therefore sum of percent values probably differs from 100','stadtkration-surveys').')</em></p>';
							}
							$count_answers = $wpdb->get_var('SELECT COUNT(*) FROM '.$table_name_user_answers.' WHERE survey_answer_possibility_id = '.$answer_row->id);
							$output .= '
			<p><strong>'.$answer_row->survey_answer_possibility_title.': '.$count_answers.'</strong><br />';
							$has_answer_possibilities = true;
							$count_users_new = $count_users;
							if(!$count_users_new) $count_users_new = $count_tokens;
							$percent = $count_answers / $count_tokens * 100;
							$output .= '<span class="percent-bar"><span class="inner" style="width:'.round($percent,2).'%"></span><span class="content">'.round($percent,2).'% </span></span></p>';
						}
					}
				}
			$output .= '
		</div>';
		}
	}
	
	// ### SHOW SURVEY FORM
	else {
	
		$login_required = false;
		if(isset($plugin_options['login-required'])) if($plugin_options['login-required']=='on') $login_required = true;
		if(($login_required && is_user_logged_in()) || !$login_required) {
						
			$output .= '
		<div class="stadtkreation-survey-frontend-form'.(!$_POST['stadtkreation-surveys-user-form-sent'] ? ' filling' : '').'">';
			
			$double_voting_allowed = false;
			if(isset($plugin_options['double-voting-allowed'])) if($plugin_options['double-voting-allowed']=='on') $double_voting_allowed = true;
			
			$can_vote = true;
			$double_submit = false;
			
			// checking for id of current user in answer table
			if (!$double_voting_allowed) {
				$user_in_list = $wpdb->get_var('SELECT COUNT(*) FROM '.$table_name_user_answers.' WHERE survey_user_id = '.wp_get_current_user()->ID);
				if($user_in_list) $can_vote = false;
			}
			
			// double submit check for not logged-in users
			else {
				if(isset($_POST['stadtkreation-surveys-unique-id'])) {
					$token_check_sql = 'SELECT survey_answer_unique_token FROM '.$table_name_user_answers.' WHERE survey_answer_unique_token = "'.$_POST['stadtkreation-surveys-unique-id'].'" LIMIT 1';
					$token_check = $wpdb->get_var($token_check_sql);
					if($token_check) $can_vote = false;
					$double_submit = true;
				}
			}
			
			if($can_vote) {
			
				// handle sent user data
				
				if(isset($_POST['stadtkreation-surveys-user-form-sent'])) {
					/* DISABLE ERROR CHECK - BUGFIX, RESOLVE LATER
					// error messages for required questions
					if(isset($_POST['required-question'])) {
						foreach($_POST['required-question'] as $question_id) {
							// error check for text fields
							if(isset($_POST['stadtkreation-user-answer-text'][$question_id])) {
								if(trim($_POST['stadtkreation-user-answer-text'][$question_id])=='') {
									$err[$question_id]['missing'] = __('Please answer this question','stadtkreation-surveys');
									$err_display[] = $question_id;
								}
							}
							// error check for checkboxes/radiobuttons
							else {
								if(!isset($_POST['stadtkreation-user-answer-id'][$question_id])) {
									$err[$question_id]['missing'] = __('Please answer this question','stadtkreation-surveys');
									$err_display[] = $question_id;
								}
							}
						}
					}
					// error messages for number of selected choices exceeding maximum allowed
					if(isset($_POST['select-max-3'])) {
						foreach($_POST['select-max-3'] as $question_id) {
							if(is_array($_POST['stadtkreation-user-answer-id'][$question_id]) && sizeof($_POST['stadtkreation-user-answer-id'][$question_id]) > 3) {
								$err[$question_id]['exceeded'] = __('Please reduce the number of selected choices for this question (max. 3 selections)','stadtkreation-surveys');
								$err_display[] = $question_id;
								
							}
						}
					}
					*/
					
					// reCAPTCHA response handling
					if(isset($plugin_options['recaptcha-website-key']) && isset($plugin_options['recaptcha-secret-key'])) if($plugin_options['recaptcha-website-key'] && $plugin_options['recaptcha-secret-key']) {
						  $captcha;
						  if(isset($_POST['g-recaptcha-response'])) $captcha=$_POST['g-recaptcha-response'];
						  if(!$captcha) $err['no-captcha-response'] = __('Please confirm that you\'re not a roboter','stadtkreation-surveys');

						  $response=file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$plugin_options['recaptcha-secret-key'].'&response='.$captcha.'&remoteip='.$_SERVER['REMOTE_ADDR']);

						  if($response.success==false) $err['captcha-error'] = __('Your are a spammer!','stadtkreation-surveys');
					}
				}
				
				// set variables for user e.g. in notification email (filled within foreach loop below)
				$submit_survey_id = '';
				if(isset($_POST['stadtkreation-surveys-user-form-sent']) && !$err) {
					$success = false;
					if(isset($_POST['stadtkreation-user-answer-id'])) {
						foreach($_POST['stadtkreation-user-answer-id'] as $key => $values) {
							if(!is_array($values)) $all_values = array($values);
							else $all_values = $values;
							foreach($all_values as $value) {
								$question_sql = 'SELECT * FROM '.$table_name_questions.' WHERE id = '.$key;
								$question_row = $wpdb->get_row($question_sql);
								$answer_sql = 'SELECT * FROM '.$table_name_answer_possibilites.' WHERE id = '.$value;
								$answer_row = $wpdb->get_row($answer_sql);
								$data = array();
								$data['survey_user_answer_creation_date'] = date('Y-m-d H:i',current_time( 'timestamp' ));
								$data['survey_id'] = $answer_row->survey_id;
								if(!$submit_survey_id) $submit_survey_id = $answer_row->survey_id;
								$data['survey_question_id'] = $key;
								$data['survey_question_title'] = $question_row->survey_question_title;
								$data['survey_answer_possibility_id'] = $value;
								$data['survey_answer_possibility_text'] = $answer_row->survey_answer_possibility_title;
								if($login_required) $data['survey_user_id'] = wp_get_current_user()->ID;
								$data['survey_answer_unique_token'] = $_POST['stadtkreation-surveys-unique-id'];
								$inserted = $wpdb->insert( $table_name_user_answers, $data);
								if($inserted) $success = true;
								else $success = false;
							}
						}
					}
					if(isset($_POST['stadtkreation-user-answer-text'])) {
						foreach($_POST['stadtkreation-user-answer-text'] as $key => $value) {
							if(trim($value)!='') {
								$question_sql = 'SELECT * FROM '.$table_name_questions.' WHERE id = '.$key;
								$question_row = $wpdb->get_row($question_sql);
								$data = array();
								$data['survey_user_answer_creation_date'] = date('Y-m-d H:i',current_time( 'timestamp' ));
								$data['survey_id'] = $question_row->survey_id;
								if(!$submit_survey_id) $submit_survey_id = $answer_row->survey_id;
								$data['survey_question_id'] = $key;
								$data['survey_question_title'] = $question_row->survey_question_title;
								$data['survey_answer_possibility_text'] = stripslashes($value);
								if($login_required) $data['survey_user_id'] = wp_get_current_user()->ID;
								$data['survey_answer_unique_token'] = $_POST['stadtkreation-surveys-unique-id'];
								$inserted = $wpdb->insert( $table_name_user_answers, $data);
								if($inserted) $success = true;
								else $success = false;
							}
						}
					}
					if($success) $output .= '
						<div class="message success-message">'.__('Thank you! Your data has been saved successfully.','stadtkreation-surveys').'</div>';
						
					// send admin mail notification if activated
					if($success && isset($plugin_options['send-notification'])) if($plugin_options['send-notification']=='on') {
						$survey_sql = 'SELECT * FROM '.$table_name_surveys.' WHERE id = '.$submit_survey_id;
						$survey_row = $wpdb->get_row($survey_sql);
						$question_sql = 'SELECT * FROM '.$table_name_questions.' WHERE survey_id = '.$submit_survey_id;
						$question_rows = $wpdb->get_results($question_sql);
						$answer_sql = 'SELECT * FROM '.$table_name_answer_possibilites.' WHERE survey_id = '.$submit_survey_id;
						$answer_rows = $wpdb->get_results($answer_sql);
						global $current_user;
						get_currentuserinfo();
						$username = $current_user->user_login;
						$usermail = $current_user->user_email;
						$to = get_option('admin_email');
						$headers = 'From: '.get_option('blogname').' '.__('Message service','stadtkreation-surveys')." <noreply@".stadtkreation_surveys_maildomain().">" . "\r\n";
						$subject = '['.get_option('blogname').'] '.sprintf(__('New user reply to survey "%s"','stadtkreation-surveys'),$survey_row->survey_title);
						$message = sprintf(__('An new reply has been submitted for survey "%s".','stadtkreation-surveys'),$survey_row->survey_title)."\r\n\r\n";
						$message .= __('User name','stadtkreation-surveys').': ' . $username . "\r\n";
						$message .= __('User email','stadtkreation-surveys').': ' . $usermail . "\r\n\r\n";
						$admin_url = admin_url();
						$options_url = $admin_url.'options-general.php?page=stadtkreation-surveys-preferences';
						$message .= '*'.__('See the current survey results within the respective section here','stadtkreation-surveys').'*'."\r\n";
						$message .= $options_url."\r\n\r\n";
						$message .= '*'.__('The following replies have been submitted','stadtkreation-surveys').'*'."\r\n\r\n";
						foreach($question_rows as $question_row) {
							$current_answer = '';
							foreach($answer_rows as $answer_row) {
								if($answer_row->survey_question_id == $question_row->id && (isset($_POST['stadtkreation-user-answer-id']))) {
									// for single selections
									if(in_array($answer_row->id,$_POST['stadtkreation-user-answer-id'])) {
										$current_answer = $answer_row->survey_answer_possibility_title;
									}
									// for multiple selections
									foreach($_POST['stadtkreation-user-answer-id'] as $key => $value) {
										if(is_array($value)) {
											if(in_array($answer_row->id,$value)) {
												if($current_answer) $current_answer .= ', ';
												$current_answer .= $answer_row->survey_answer_possibility_title;
											}
										}
									}
								}
							}
							if(!$current_answer) $current_answer = $_POST['stadtkreation-user-answer-text'][$question_row->id];
							if($current_answer) {
								$message .= __('Question: ','stadtkreation-surveys').$question_row->survey_question_title."\r\n";
								$message .= __('Answer: ','stadtkreation-surveys').stripslashes($current_answer)."\r\n\r\n";
							}
						}
						$message_sent = wp_mail($to,$subject,$message,$headers);
					}
				}
				
				// output survey form (and error messages, if existing)
				if(!$success) {
					if($err['captcha_error']) $output .= '
							<div class="message error-message">
								'.$err['captcha_error'].'
							</div>';
					else if($err) $output .= '
							<div class="message error-message">
								'.__('Your answers could not be saved. Please view the marked questions for errors.','stadtkreation-surveys').'
							</div>';
					$survey_sql = 'SELECT * FROM '.$table_name_surveys.' WHERE id = '.$a['id'].' ORDER BY survey_creation_date ASC';
					$survey_rows = $wpdb->get_results($survey_sql);
					foreach($survey_rows as $survey_row) {
						if(in_array('showtitle',$atts)) $output .= '
						<h2>'.$survey_row->survey_title.'</h2>';
						$output .= '
						<form method="post">';
						
						if($err['captcha_error'] || $err['no-captcha-response']) {  // BUGFIX, REMOVE LATER
							$output .= '
							<div style="display:none">';
						}
						$question_sql = 'SELECT * FROM '.$table_name_questions.' WHERE survey_id = '.$a['id'].' ORDER BY survey_question_order ASC';
						$question_rows = $wpdb->get_results($question_sql);
						$question_count = 1;
						$next_question_id = 0;
						foreach($question_rows as $question_row) {
							if(isset($question_rows[$question_count])) $next_question_id=$question_rows[$question_count]->id;
							/* QUICK BUGFIX FOR LAST QUESTION -- RESOLVE LATER */ if($question_row->id == $next_question_id) $next_question_id = '';
							$question_count++;
							if($question_row->survey_question_title_before) $output .= '
							<h2 class="survey-section-title">'.$question_row->survey_question_title_before.'</h2>';
							$output .= '
							<div class="question-box'.($question_row->survey_question_optional!=1 ? ' required' : '').' handle" data-id="'.$question_row->id.'"'.($question_row->survey_question_next_question_default > 0 ? ' data-next-selected="'.$question_row->survey_question_next_question_default.'"' : '').'>';
							$output .= '
								<div'.((isset($err_display) && in_array($question_row->id,$err_display)) ? ' class="error"' : '').'>';
							$output .= '	
									<h3>'.$question_row->survey_question_title.($question_row->survey_question_optional==1 ? ' <em class="optional">'.__('(optional)','stadtkreation-survey').'</em>' : '').'</h3>';
							if($question_row->survey_question_intro) $output .= '
									<p>'.$question_row->survey_question_intro.'</p>';
							if($question_row->survey_question_type == 'checkbox') $output .= '
									<p><em>'.__('Multiple selection possible','stadtkreation-surveys').'</em></p>';
							if($question_row->survey_question_type == 'checkbox3') $output .= '
									<p><em>'.__('Multiple selection possible, max. 3 selections','stadtkreation-surveys').'</em></p>';	
							
							// error text output
							if(isset($err_display) && in_array($question_row->id,$err_display)) {
								foreach($err[$question_row->id] as $error_output) $output .= '
									<p class="error">'.$error_output.'</p>';
							}
							
							$answer_sql = 'SELECT * FROM '.$table_name_answer_possibilites.' WHERE survey_question_id = '.$question_row->id.' ORDER BY survey_answer_possibility_order ASC';
							$answer_rows = $wpdb->get_results($answer_sql);
							$output .= '
								<p>';
							if($answer_rows) {
								
								
								// handle question types field output
								if($question_row->survey_question_type == 'checkbox' || $question_row->survey_question_type == 'checkbox3') {
									foreach($answer_rows as $answer_row) {
										// get IDs for next questions, if existing
										$data_next =$next_question_id;
										if($question_row->survey_question_next_question_default) $data_next = $question_row->survey_question_next_question_default;
										if($answer_row->survey_answer_possibility_next_question) $data_next = $answer_row->survey_answer_possibility_next_question;
										
										$output .= '
										<label><input'.($data_next ? ' data-next="'.$data_next.'"' : '').' type="checkbox" name="stadtkreation-user-answer-id['.$question_row->id.'][]" value="'.$answer_row->id.'"'.(isset($_POST['stadtkreation-user-answer-id'][$question_row->id]) && ($_POST['stadtkreation-user-answer-id'][$question_row->id] == $answer_row->id || (is_array($_POST['stadtkreation-user-answer-id'][$question_row->id]) && in_array($answer_row->id, $_POST['stadtkreation-user-answer-id'][$question_row->id]))) ? ' checked="checked"' : '' ).' /> '.$answer_row->survey_answer_possibility_title.'</label><br />';
									}
									if($question_row->survey_question_type == 'checkbox3') $output .= '
										<input type="hidden" name="select-max-3[]" value="'.$question_row->id.'" />';
									if($question_row->survey_question_optional!=1) $output .= '
										<input type="hidden" name="required-question[]" value="'.$question_row->id.'" />';
								}
								else {
									foreach($answer_rows as $answer_row) {
										// get IDs for next questions, if existing
										$data_next = $next_question_id;
										if($question_row->survey_question_next_question_default) $data_next = $question_row->survey_question_next_question_default;
										if($answer_row->survey_answer_possibility_next_question) $data_next = $answer_row->survey_answer_possibility_next_question;
										
										$output .= '
										<label><input'.($data_next ? ' data-next="'.$data_next.'"' : '').' type="radio" name="stadtkreation-user-answer-id['.$question_row->id.']" value="'.$answer_row->id.'"'.(isset($_POST['stadtkreation-user-answer-id'][$question_row->id]) && $_POST['stadtkreation-user-answer-id'][$question_row->id] == $answer_row->id ? ' checked="checked"' : '' ).' /> '.$answer_row->survey_answer_possibility_title.'</label><br />';
									}
									if($question_row->survey_question_optional!=1) $output .= '
										<input type="hidden" name="required-question[]" value="'.$question_row->id.'" />';
								}
							}
							else {
								if($question_row->survey_question_type == 'textarea') $output .= '
										<textarea name="stadtkreation-user-answer-text['.$question_row->id.']">'.(isset($_POST['stadtkreation-user-answer-text'][$question_row->id]) && $_POST['stadtkreation-user-answer-text'][$question_row->id]!='' ? stripslashes($_POST['stadtkreation-user-answer-text'][$question_row->id]) : '').'</textarea>';
								else $output .= '
										<input type="text" name="stadtkreation-user-answer-text['.$question_row->id.']"'.(isset($_POST['stadtkreation-user-answer-text'][$question_row->id]) && $_POST['stadtkreation-user-answer-text'][$question_row->id]!='' ? ' value="'.stripslashes($_POST['stadtkreation-user-answer-text'][$question_row->id].'"') : '').' />';
									if($question_row->survey_question_optional!=1) $output .= '
										<input type="hidden" name="required-question[]" value="'.$question_row->id.'" />';
							}
							$output .= '
									</p>
								</div>
							</div>';
						}
						$unique_id = uniqid();
						if(!isset($_POST['stadtkreation-surveys-user-form-sent'])) $output .= '
							<p>
								<button class="show-next-question inactive">'.__('Next question','stadtkreation-surveys').'<span class="index"></span></button>
							</p>';
						if($err['captcha_error'] || $err['no-captcha-response']) {  // BUGFIX, REMOVE LATER
							$output .= '
							</div>';
						}
						if(isset($plugin_options['recaptcha-website-key']) && isset($plugin_options['recaptcha-secret-key'])) if($plugin_options['recaptcha-website-key'] && $plugin_options['recaptcha-secret-key']) $output .= '
							<div class="recaptcha'.(isset($err['no-captcha-response']) ? ' error' : '').'">'.(isset($err['no-captcha-response']) ? '<p class="error">'.$err['no-captcha-response'].'</p>' : '').'<div class="g-recaptcha" '.(isset($err['no-captcha-response']) ? 'style="display:block !important"' : '').'data-sitekey="'.$plugin_options['recaptcha-website-key'].'"></div></div>';
						$output .= '
							<p>
								<input type="hidden" name="stadtkreation-surveys-user-form-sent" value="1" />
								<input type="hidden" name="stadtkreation-surveys-unique-id" value="'.$unique_id.'" />
								<input class="stadtkreation-submit-survey'.(!isset($_POST['stadtkreation-surveys-user-form-sent']) ? ' inactive' : '').'" type="submit" value="'.__('Submit data','stadtkreation-surveys').'" />
							</p>
						</form>';
					}
				}
			}
			else {
				if(!$double_submit) $output .= '
						<div class="message error-message">'.__('You have already participated in this survey.','stadtkreation-surveys').'</div>';
				else $output .= '
						<div class="message error-message">'.__('Your answers have already been saved on a previous submit. Maybe you pressed the reload button of your browser directly after submitting your data.','stadtkreation-surveys').'</div>';
			}
			$output .= '
		</div>';
		}
		else $output = $content;
	}
	
	return $output;
}
add_shortcode('stadtkreation-survey','output_stadtkreation_survey');

// shortcode [form-not-sent]
function output_form_not_sent($atts,$content) {
	if(!isset($_POST['stadtkreation-surveys-user-form-sent'])) return do_shortcode(wpautop($content));
	else return;
}
add_shortcode('form-not-sent','output_form_not_sent');

// create domain output for automatically created e-mail addresses like noreply@...
if(!function_exists('stadtkreation_surveys_maildomain')) {
	function stadtkreation_surveys_maildomain() {
		$maildomain = str_replace('http://','',get_bloginfo('url'));
		$maildomain = str_replace('https://','',$maildomain);
		$maildomain = str_replace('www.','',$maildomain);
		$maildomain = explode('/',$maildomain);
		$maildomain = $maildomain[0];
		return $maildomain;
	}
}

// export to csv functionality
function stadtkreation_surveys_download_csv() {
	if(isset($_POST['stadtkreation_surveys_download_csv'])) {
		global $wpdb;
		$table_name_no_prefix = 'stadtkreation_survey_user_answers';
		$table_name = $wpdb->prefix . $table_name_no_prefix;
		
		// USE LATER FOR PLAIN RESULTS EXPORT
		$old_sql = 'SELECT survey_user_answer_creation_date AS "'.__('Date','stadtkreation-surveys').'", survey_question_id AS "'.__('Question ID','stadtkreation-surveys').'", survey_question_title AS "'.__('Question title','stadtkreation-surveys').'", survey_answer_possibility_id AS "'.__('Answer possibility ID','stadtkreation-surveys').'", survey_answer_possibility_text AS "'.__('Answer possibility','stadtkreation-surveys').'", survey_user_id AS "'.__('User ID','stadtkreation-surveys').'", survey_answer_unique_token AS "'.__('Unique token','stadtkreation-surveys').'" FROM '.$table_name.' WHERE survey_id = '.$_POST['csv_survey_id'].' ORDER BY survey_user_answer_creation_date ASC';
		$old_rows = $wpdb->get_results($old_sql, 'ARRAY_A');
		
		$sql = 'SELECT survey_user_answer_creation_date, survey_question_id, survey_question_title, survey_answer_possibility_id, survey_answer_possibility_text, survey_user_id, survey_answer_unique_token FROM '.$table_name.' WHERE survey_id = '.$_POST['csv_survey_id'].' ORDER BY survey_user_answer_creation_date ASC';
		$rows = $wpdb->get_results($sql);
		
		if($rows) {
			$output_filename = 'stadtkreation_surveys_export_'.date('Y-m-d_H-i-s',current_time( 'timestamp' )).'_survey'.$_POST['csv_survey_id'].'.csv';
			$output_handle = @fopen('php://output', 'w');

			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Content-Description: File Transfer');
			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename=' . $output_filename);
			header('Expires: 0');
			header('Pragma: public');
			
			$titles_1 = array();
			$titles_2 = array();
			$titles_3 = array();
			$question_ids = array();
			$answer_possibility_ids = array();
			$answer_fields = array();
			
			// get section titles, question titles, and answer possibility titles for 3 title rows
			$table_name_no_prefix = 'stadtkreation_survey_questions';
			$table_name = $wpdb->prefix . $table_name_no_prefix;
			$table_name_no_prefix_answer_possibilities = 'stadtkreation_survey_answer_possibilities';
			$table_name_answer_possibilities = $wpdb->prefix . $table_name_no_prefix_answer_possibilities;
			$sql = 'SELECT id, survey_question_title_before, survey_question_title FROM '.$table_name.' WHERE survey_id = "'.$_POST['csv_survey_id'].'" ORDER BY survey_question_order ASC';
			$rows_question_titles = $wpdb->get_results($sql);
			
			// fixed rows
			$titles_2[] = __('Date','stadtkreation_surveys'); $titles_1[] = ''; $titles_3[] = ''; $question_ids[] = ''; $answer_possibility_ids[] = '';
			
			// populate three title rows and set array with question ids and answer possibility ids
			foreach ($rows_question_titles as $row) {
				$titles_1[] = strip_tags($row->survey_question_title_before);
				$titles_2[] = strip_tags($row->survey_question_title);
				$sql = 'SELECT id, survey_answer_possibility_title FROM '.$table_name_answer_possibilities.' WHERE survey_id = '.$_POST['csv_survey_id'].' AND survey_question_id = '.$row->id.' ORDER BY survey_answer_possibility_order ASC';
				$rows_answer_possibilities = $wpdb->get_results($sql);
				if($rows_answer_possibilities) {
					$first = true;
					foreach ($rows_answer_possibilities as $row_answer_possibilities) {
						if(!$first) {
							$titles_1[] = '';
							$titles_2[] = '';
						}
						$titles_3[] = strip_tags($row_answer_possibilities->survey_answer_possibility_title);
						$question_ids[] = $row->id;
						$answer_possibility_ids[] = $row_answer_possibilities->id;
						$first = false;
					}
				}
				else {
					$titles_3[] = '';
					$question_ids[] = $row->id;
					$answer_possibility_ids[] = '';
				}
			}
			
			// parse three title rows to CSV format
			fputcsv($output_handle, $titles_1);
			fputcsv($output_handle, $titles_2);
			fputcsv($output_handle, $titles_3);

			$current_token = '';
			$first = true;
			$answer_fields[] = '';
			$count = 0;
			$old_creation_date = '';
			foreach($answer_possibility_ids as $answer_possibility_id) {
				$answer_fields[$count] = '';
				$count++;
			}
			foreach ($rows as $row) {
				if($first) {
					$current_token = $row->survey_answer_unique_token;
					$first = false;
				}
				
				// write new line with respective results to CSV and reset results array
				if($row->survey_answer_unique_token != $current_token) {
					$current_token = $row->survey_answer_unique_token;
					$answer_fields[0] = $old_creation_date;
					fputcsv($output_handle, $answer_fields);
					$answer_fields = array();
					$count = 0;
					foreach($answer_possibility_ids as $answer_possibility_id) {
						$answer_fields[$count] = '';
						$count++;
					}
				}
				
				$count = 0;
				$found = false;
				$current_question_id = $row->survey_question_id;
				$current_answer_possibility_id = $row->survey_answer_possibility_id;
				
				// if multiple choice anser: loop through answer possibilities and set 1 on correct position
				if($current_answer_possibility_id) {
					foreach($answer_possibility_ids as $answer_possibility_id) {
						$found = true;
						if($question_ids[$count] == $current_question_id) {
							if($answer_possibility_id == $current_answer_possibility_id) {
								$answer_fields[$count] = '1'; // use = $current_answer_possibility_id here for debugging
							}
						}
						$count++;
					}
				}
				
				// if free text answer: loop through question and set user answer text on correct position
				else {
					$count = 0;
					foreach($question_ids as $question_id) {
						if($question_id == $current_question_id) $answer_fields[$count] = $row->survey_answer_possibility_text;
						$count++;
					}
				}
				$old_creation_date = $row->survey_user_answer_creation_date;
				
			}
			$answer_fields[0] = $row->survey_user_answer_creation_date;
			fputcsv($output_handle, $answer_fields);

			/* OLD
			// parse results to csv format
			$first = true;
			foreach ($rows as $row) {

				// add table headers
				if($first) {
					$titles = array();
					foreach($row as $key => $val) $titles[] = $key;
					fputcsv($output_handle, $titles);
					$first = false;
				}
				
				// add row to CSV file
				$row_content_array = (array) $row;				
				fputcsv($output_handle, $row_content_array);
			}
			*/

			// close output file stream
			fclose($output_handle);

			die();
		}
	}
}
add_action('admin_init', 'stadtkreation_surveys_download_csv');

?>