STADTKREATION Surveys plugin for flexible surveys.

Version 0.5 - 2020-06-21
------------------------

added several new features

Version 0.4 - 2020-06-21
------------------------

added several new features

Version 0.3 - 2020-06-21
------------------------

added several new features

Version 0.2 - 2020-06-21
------------------------

added several new features

Version 0.1 - 2020-06-21
------------------------

initial commit