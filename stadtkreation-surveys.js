// JS functions for Stadtkreation Surveys plugin by Johannes Bouchain - 02/2018

var $ = jQuery;
var reloaded = true;

$(document).ready(function(){
	// initially show first question of survey on initial screen (= .filling) (not error screen)
	$('.stadtkreation-survey-frontend-form.filling .question-box').first().show();
	
	// call answer change handler
	$('.stadtkreation-survey-frontend-form .question-box').find('input, textarea').on('change keypress keyup keydown',function(){
		// prevent too many answers for "max 3" checkbox questions
		if($(this).closest('.question-box').find('input[name="select-max-3[]"]').length) {
			if($(this).closest('.question-box').find('input:checked').length>3) {
				$(this).prop('checked',false);
				// TODO: Make translation friendly!!!
				alert('Sie dürfen maximal 3 Antwortmöglichkeiten auswählen. Bitte entfernen Sie ein anderes Häkchen um hier eines zu setzen.');
			}
		}		
		checkCurrentAnswer($(this));
		checkSectionTitles();
	});
	
	// initially trigger all event handlers for checked boxes and input fields/text areas with values
	$('.stadtkreation-survey-frontend-form input, .stadtkreation-survey-frontend-form textarea, .stadtkreation-survey-frontend-form select').each(function(){
		if($(this).attr('type')!='submit' && $(this).attr('type')!='hidden') {
			if($(this).attr('type')=='checkbox' || $(this).attr('type')=='radio') {
				if($(this).prop('checked')) $(this).trigger('change');
			}
			else if($(this).val()!='') $(this).trigger('keypress');
		}
	});
	
	// enable submit directly if survey has only one question
	if($('.stadtkreation-survey-frontend-form.filling .question-box').length==1) {
		$('.stadtkreation-survey-frontend-form.filling .show-next-question').closest('p').hide();
	}
		
	// enable next question button if last visible question is not required
	if(!$('.stadtkreation-survey-frontend-form.filling .question-box:visible:last').hasClass('required')) $('.stadtkreation-survey-frontend-form.filling .show-next-question').removeClass('inactive');
	
	// add next question on respective button click
	$('.stadtkreation-survey-frontend-form.filling .show-next-question').on('click',function(e){
		e.preventDefault();
		if(!$(this).hasClass('inactive')) {
			if($('.stadtkreation-survey-frontend-form.filling .question-box:visible:last').attr('data-next-selected')) {
				$('.stadtkreation-survey-frontend-form.filling .question-box[data-id="'+$('.stadtkreation-survey-frontend-form.filling .question-box:visible:last').attr('data-next-selected')+'"]').slideDown();
			}
			else {
				if($('.stadtkreation-survey-frontend-form.filling .question-box:visible:last').next().hasClass('survey-section-title')) $('.stadtkreation-survey-frontend-form.filling .question-box:visible:last').next('.survey-section-title').next('.question-box.handle:hidden:first').slideDown();
				else $('.stadtkreation-survey-frontend-form.filling .question-box:visible:last').next('.question-box.handle:hidden:first').slideDown();
			}
			
			handleSubmitButton();				
			
			var lastVisible = $('.stadtkreation-survey-frontend-form.filling .question-box:visible:last').index()+1;
			//for(var i=1;i<=lastVisible;i++) $('.stadtkreation-survey-frontend-form.filling .question-box:nth-child('+i+')').addClass('has-handled');

			
			// disable next question button if last visible question is required
			if($('.stadtkreation-survey-frontend-form.filling .question-box:visible:last').hasClass('required')) $('.stadtkreation-survey-frontend-form.filling .show-next-question').addClass('inactive');
			
			var checkFields = $('.question-box:visible:last').find('input[type="checkbox"]:checked, input[type="radio"]:checked');
			if(checkFields.length) {
				checkFields.each(function(){
					$(this).trigger('change');
				});
			}
			var inputField = $('.question-box:visible:last').find('input[type="text"], textarea');
			if(inputField.val()!='') inputField.trigger('keypress');
		}
		checkSectionTitles();
	});
	
	checkSectionTitles();		
	
	// prevent default on submit
	$(document).on('submit','.stadtkreation-survey-frontend-form.filling form',function(e){
		if($('.stadtkreation-survey-frontend-form .stadtkreation-submit-survey.inactive').length) {
			e.preventDefault()
		}
	});
	
	reloaded = false;
	
	handleSubmitButton();
	
});

var setInactive, noneChecked;
function checkCurrentAnswer(elem) {
	setInactive = false;
	noneChecked = false;
	if(elem.attr('type') == 'radio' || elem.attr('type') == 'checkbox') {
		if(elem.prop('checked')) {
			// important variables for conditional questions
			// find('input:checked').last() --> change to first() for reverse condition priority
			var nextQuestionIndex = $('.question-box[data-id="'+elem.closest('.question-box').find('input:checked').last().attr('data-next')+'"]').first().index()+1;
			if(!nextQuestionIndex) nextQuestionIndex = elem.closest('.question-box').index()+2;
			var oldNextQuestionIndex =  $('.question-box[data-id="'+elem.closest('.question-box').attr('data-next-selected')+'"]').index()+1;
			if(!oldNextQuestionIndex) oldNextQuestionIndex = elem.closest('.question-box').index()+2;
			// last() --> change to first() for reverse condition priority
			elem.closest('.question-box').attr('data-next-selected',elem.closest('.question-box').find('input:checked').last().attr('data-next'));
			var startCheck = oldNextQuestionIndex;
			var endCheck = nextQuestionIndex;
			if(endCheck<startCheck) {
				startCheck = nextQuestionIndex;
				endCheck = oldNextQuestionIndex;
			}
			if(startCheck != endCheck) {
				var isFirst = true;
				for(var i=startCheck;i<endCheck;i++) {
					var currElem = $('.question-box:nth-child('+i+')');
					// this is the original line, the next line is a bugfix: if(currElem.hasClass('handle')) {
					if(currElem.hasClass('handle') && currElem.is(':visible')) {
						currElem.removeClass('handle');
						currElem.removeClass('has-handled');
						currElem.removeClass('handler-added');
						currElem.slideUp();
						if(!reloaded) {
							currElem.find('input[type="checkbox"], input[type="radio"]').prop('checked', false);
							currElem.find('input[type="text"]').val('');
						}
						if($('.handler-added').length) {
							elem.nextAll('.question-box').removeClass('handler-added');
							elem.nextAll('.question-box.handler-added').removeClass('handle');
						}
						currElem.find('input, textarea, select').each(function(){
							$(this).attr('name','deactivated-'+$(this).attr('name'));
						});
					}
					else {
						currElem.addClass('handle');
						currElem.addClass('handler-added'); // class to mark questions that are to be handled because of question dependencies
						if($('.stadtkreation-survey-frontend-form.filling').length) {
							if(currElem.hasClass('has-handled') && isFirst) currElem.slideDown();
						}
						else currElem.slideDown();
						isFirst = false;
						currElem.find('input, textarea, select').each(function(){
							$(this).attr('name',$(this).attr('name').replace('deactivated-',''));
						});
					}
				}
				if($('.stadtkreation-survey-frontend-form.filling').length) {
					for(var i = $('.stadtkreation-survey-frontend-form .question-box').length;i >= endCheck-1; i--) {
						var currElem = $('.question-box:nth-child('+i+')');
						currElem.slideUp();
						if(!reloaded) {
							currElem.find('input[type="checkbox"], input[type="radio"]').prop('checked', false);
							currElem.find('input[type="text"]').val('');
						}
						if(!currElem.hasClass('required')) setInactive = true;
						currElem.removeClass('has-handled');
					}
				}
			}
		}
		
		// last() --> change to first() for reverse condition priority
		else elem.closest('.question-box').find('input:checked').last().trigger('change');
		
		// remove handler that has been dynamically added for not visible questions
		if($('.handler-added').length) {
			$('.question-box').not(':visible').removeClass('handler-added');
			$('.question-box.handler-added').not(':visible').removeClass('handle');
		}
		
		// check if no option selected and hide/empty next questions if already answered
		if(elem.attr('type') == 'checkbox') {
			if(elem.closest('.question-box').find('input:checked').length==0 && elem.closest('.question-box').hasClass('required')) {
				elem.closest('.question-box').nextAll('.question-box').each(function(){
					$(this).removeClass('handler-added');
					$(this).slideUp();
					setInactive = true;
					noneChecked = true;
					if(!reloaded) {
						$(this).find('input[type="checkbox"], input[type="radio"]').prop('checked', false);
						$(this).find('input[type="text"]').val('');
					}
				});
			}
		}
	}
	
	else {
		if(!elem.val().length && elem.hasClass('required')) {
			setInactive = true;
		}
	}
	
	// enable/disable next question button (only if last visible question has been changed)
	setTimeout(function(){
		if(elem.closest('.question-box').nextAll('.question-box:visible').length==0) {
			if(!noneChecked) if($('.question-box:visible:last input:checked').length) setInactive = false;
			if(!setInactive) $('.stadtkreation-survey-frontend-form.filling .show-next-question').removeClass('inactive');
			else $('.stadtkreation-survey-frontend-form.filling .show-next-question').addClass('inactive');
			console.log(setInactive);
		}
	},600);
	
	// activate submit button if last question has been changed
	handleSubmitButton();

}
function checkSectionTitles() {
	setTimeout(function(){
		$('h2.survey-section-title').each(function(){
			var showCurrentTitle = false;
			stopNow = false;
			$(this).nextAll('.question-box').each(function(){
				if($(this).is(':visible')) showCurrentTitle = true;
				if($(this).next().hasClass('survey-section-title') && !showCurrentTitle) {
					stopNow = true;
					return;
				}
			});
			if(!stopNow) if(showCurrentTitle && $(this).is(':hidden')) $(this).slideDown();
			if(!showCurrentTitle && $(this).is(':visible')) $(this).slideUp();
		});
	},550);
}

function handleSubmitButton() {
	if($('.stadtkreation-survey-frontend-form.filling').length) {
		if((!$('.stadtkreation-survey-frontend-form.filling .question-box.handle:hidden').length || $('.stadtkreation-survey-frontend-form.filling .question-box:last').is(':visible')) && !$('.stadtkreation-survey-frontend-form.filling .question-box.has-handled:hidden').length) {
			$('.stadtkreation-survey-frontend-form.filling .show-next-question').closest('p').hide();
			$('.stadtkreation-survey-frontend-form.filling .stadtkreation-submit-survey').show();
		}
		else {
			$('.stadtkreation-survey-frontend-form.filling .show-next-question').closest('p').show();
			$('.stadtkreation-survey-frontend-form.filling .stadtkreation-submit-survey').hide();
		}
	}
	var activateSubmit = false 
	if($('.stadtkreation-survey-frontend-form .question-box:last').is(':visible')) {
		if($('.stadtkreation-survey-frontend-form .question-box:last').hasClass('required')) {
			$('.stadtkreation-survey-frontend-form .question-box:last').find('input[type="checkbox"], input[type="radio"]').each(function(){
				if($(this).prop('checked')) activateSubmit = true;
			});
			if($('.stadtkreation-survey-frontend-form .question-box:last').find('input[type="text"], textarea').val()) activateSubmit = true;
		}
		else activateSubmit = true;
	}
	if($('.recaptcha.error').length) { // BUGFIX, REMOVE LATER
		activateSubmit = true;
	}
	if(activateSubmit) {
		$('.stadtkreation-survey-frontend-form .stadtkreation-submit-survey').removeClass('inactive');
		$('.stadtkreation-survey-frontend-form .g-recaptcha').show();	
	}
	else {
		$('.stadtkreation-survey-frontend-form .stadtkreation-submit-survey').addClass('inactive');
		$('.stadtkreation-survey-frontend-form .g-recaptcha').hide();
	}

		
}
