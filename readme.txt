=== STADTKREATION Surveys ===

Contributors: STADTKREATION

Requires at least: 4.5
Tested up to: 5.4.2
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: LICENSE

STADTKREATION Surveys plugin for flexible surveys.

== Description ==

Description

== Installation ==

1. In your admin panel, go to Appearance > Plugins and click the Add New button.
2. Click Upload Plugin and Choose File, then select the plugins's .zip file. Click Install Now.
3. Click Activate to use your new plugin right away.

== Changelog ==

Version 0.5 - 2020-06-21
------------------------

added several new features

Version 0.4 - 2020-06-21
------------------------

added several new features

Version 0.3 - 2020-06-21
------------------------

added several new features

Version 0.2 - 2020-06-21
------------------------

added several new features

Version 0.1 - 2020-06-21
------------------------

initial commit

== Credits ==

* Developed by STADTKREATION https://stadtkreation.de/about-us/, (C) 2020 STADTKREATION, [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)